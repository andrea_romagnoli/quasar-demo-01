export function LOGIN (state, payload) {
  console.log('login ' + payload.username + ',' + payload.password)
  state.isUserLogged = (payload.username === state.auth.username && payload.password === state.auth.password)
  console.log('logged ' + state.isUserLogged)
}

export function LOGOUT (state) {
  state.isUserLogged = false
  // eslint-disable-next-line indent
}
